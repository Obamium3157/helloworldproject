package com.company;

import java.util.ArrayList;

public class User {
    String name;
    String surname;
    ArrayList<Email> emails = new ArrayList<>();
    ArrayList<Phone> phones = new ArrayList<>();
    User(String name, String surname, Email email, Phone phone){
        this.name = name;
        this.surname = surname;
        this.emails.add(email);
        this.phones.add(phone);
        this.print();
    }
    public String getInfo(){
        return this.name + " " + this.surname;
    }

    public void print(){
        System.out.println(this.name + " " + this.surname);
        System.out.print("Emails: ");
        for (Email email: this.emails)
            System.out.print(email.get() + " ");
        System.out.print("Phones: ");
        for (Phone phone: this.phones)
            System.out.print(phone.get() + " ");
    }
}