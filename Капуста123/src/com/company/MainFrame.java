package com.company;

import javax.swing.*;

public class MainFrame extends JFrame {
    MainFrame(){
        setTitle("Наша программа");
        setSize(500, 500);
        setLocation(200, 200);
        add(new CreateUserPanel());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }
}