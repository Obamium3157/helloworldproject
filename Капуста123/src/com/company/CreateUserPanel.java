package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class CreateUserPanel extends JPanel {
    CreateUserPanel(){
        JTextField name = createComponent("Введите имя");
        JTextField surname = createComponent("Введите фамилию");
        JTextField email = createComponent("Введите email");
        JTextField phone = createComponent("Введите номер телефона");
        JButton createUser = new JButton("Создать");
        add(createUser);
        JLabel label = new JLabel("Введите данные, чтобы создать пользователя");
        add(label);
        Font f = new Font("Ballpark", Font.ITALIC, 25);
        JLabel label2 = new JLabel("Hello, world!");
        label2.setFont(f);
        add(label2);
        createUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User user = new User(
                        name.getText(),
                        surname.getText(),
                        new Email(email.getText()),
                        new Phone(phone.getText())
                );
                label.setText(user.getInfo());
            }
        });
    }

    public JTextField createComponent(String title){
        JTextField component = new JTextField(title);
        component.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (component.getText().equals(title))
                    component.setText("");
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (component.getText().isEmpty())
                    component.setText(title);
            }
        });
        this.add(component);
        return component;
    }
}