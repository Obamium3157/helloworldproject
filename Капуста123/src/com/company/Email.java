package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private String address;
    Email(String address){
        set(address);
    }
    public void set(String email){
        if (!isEmailValid(email)) {
            do {
                System.out.print("Ошибка ввода email! Введите еще раз: ");
                email = Main.in.nextLine();
            } while (!isEmailValid(email));
        }
        this.address = email;
    }
    public String get(){
        return this.address;
    }
    public static boolean isEmailValid(String email){
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}